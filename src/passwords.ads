with System.Unsigned_Types;
with words;

package passwords is

   function Generate
     (wordlist : String; segs : Integer := 2; sep : String := "^")
      return String;
   function Generate
     (wordlist : words.CandidateWords_Type; segs : Integer := 2;
      sep      : String := "^") return String;

end passwords;
