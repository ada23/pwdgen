with Ada.Containers.Vectors;

package words is

   debug : Boolean := False;

   MAXLENGTH : constant Integer := 16;
   subtype Word_Type is String (1 .. MAXLENGTH);
   subtype MaxWordsType is Integer range 1 .. 500_000;
   package Words_Pkg is new Ada.Containers.Vectors (MaxWordsType, Word_Type);
   type CandidateWords_Type is record
      words : Words_Pkg.Vector;
   end record;
   type StringOptions is (None, Capitalize, UpperCase, LowerCase);
   function Initialize
     (wordlist : String; maxwordlength : Integer := MAXLENGTH)
      return CandidateWords_Type;
   function Initialize
     (wordlist      : String; separator : Character;
      maxwordlength : Integer := MAXLENGTH) return CandidateWords_Type;
   function Choose
     (cw : CandidateWords_Type; option : StringOptions := None) return String;
   function Choose (cw : CandidateWords_Type) return Integer;
   procedure CodeGen
     (cw : CandidateWords_Type; pkgname : String := "words_str");

end words;
