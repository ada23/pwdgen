with GNAT.Strings;

package cli is

   VERSION : String := "V01";
   NAME    : String := "pwdgen";
   Verbose : aliased Boolean;

   HelpOption    : aliased Boolean;
   dumpOption    : aliased Boolean;
   builtinOption : aliased Boolean;
   deriveOption  : aliased Boolean;

   NumSegments   : aliased Integer := 2;
   MaxWordLength : aliased Integer := 6;
   Iterations    : aliased Integer := 3;

   Separator : aliased GNAT.Strings.String_Access;

   WordListFile        : aliased GNAT.Strings.String_Access;
   DefaultWordListFile : aliased String := "wordlist.txt";
   DefaultSeparator    : aliased String := "-";

   procedure ProcessCommandLine;
   function GetNextArgument return String;
   procedure ShowCommandLineArguments;

   function Get (prompt : String) return String;
end cli;
